import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import WS from "jest-websocket-mock";
import "@testing-library/jest-dom/extend-expect";

import OrderBook from "./OrderBook";

let ws: WS;
beforeEach(() => {
  ws = new WS("wss://www.cryptofacilities.com/ws/v1");
});
afterEach(() => {
  WS.clean();
});

test("sanity test", () => {
  render(<OrderBook />);
  expect(screen.getByText(/Order Book/i)).toBeInTheDocument();
});

test("hangle with the websocket unexpected connection close", async () => {
  render(<OrderBook />);

  await ws.connected;

  ws.close();

  expect(
    await screen.findByText(/Got an unexpected error/i)
  ).toBeInTheDocument();
});

test("hangle with the websocket messages to reconciliate the state", async () => {
  render(<OrderBook />);

  await ws.connected;

  await expect(ws).toReceiveMessage(
    '{"event":"subscribe","feed":"book_ui_1","product_ids":["PI_XBTUSD"]}'
  );
  ws.send(JSON.stringify({ event: "info", version: 1 }));
  ws.send(
    JSON.stringify({
      event: "subscribed",
      feed: "book_ui_1",
      product_ids: ["PI_XBTUSD"],
    })
  );
  ws.send(
    JSON.stringify({
      numLevels: 25,
      feed: "book_ui_1_snapshot",
      bids: [
        [33400.0, 1000.0],
        [33388.5, 5000.0],
      ],
      asks: [
        [33401.5, 2000.0],
        [33403.0, 8000.0],
      ],
      product_id: "PI_XBTUSD",
    })
  );

  expect(await screen.findByText(/33,400.00/i)).toBeInTheDocument();
  expect(screen.getByText(/33,388.50/i)).toBeInTheDocument();
  expect(screen.getByText(/33,401.50/i)).toBeInTheDocument();
  expect(screen.getByText(/33,403.00/i)).toBeInTheDocument();

  ws.send(
    JSON.stringify({
      feed: "book_ui_1",
      product_id: "PI_XBTUSD",
      bids: [
        [33388.5, 7000.0],
        [33400.0, 0],
      ],
      asks: [
        [33401.5, 0],
        [33403.0, 3200.0],
      ],
    })
  );

  await waitFor(() =>
    expect(screen.queryAllByText(/33,400.00/i)).toHaveLength(0)
  );
  expect(screen.queryAllByText(/33,401.50/i)).toHaveLength(0);
  expect(screen.queryAllByText(/5,000/i)).toHaveLength(0);
  expect(screen.queryAllByText(/8,000/i)).toHaveLength(0);
  expect(screen.queryAllByText(/3,200/i)).toHaveLength(2);
  expect(screen.queryAllByText(/7,000/i)).toHaveLength(2);
});
