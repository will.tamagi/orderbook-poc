import React, { useMemo } from "react";
import { Column, Table } from "react-virtualized";

import styles from "./VirtualizedTable.module.css";

import "react-virtualized/styles.css";

interface VirtualizedTableProps {
  data: any[];
  dataType: "bids" | "asks";
}
const VirtualizedTable: React.FC<VirtualizedTableProps> = ({
  data,
  dataType,
}) => {
  const columComponents = useMemo(() => {
    const columns = [
      <Column
        width={200}
        label="Total"
        dataKey="2"
        key={`${dataType}-total`}
        cellDataGetter={({ rowData }) =>
          Intl.NumberFormat("en-IN", {}).format(rowData[2])
        }
      />,
      <Column
        width={200}
        label="Size"
        dataKey="1"
        key={`${dataType}-size`}
        cellDataGetter={({ rowData }) =>
          Intl.NumberFormat("en-IN", {}).format(rowData[1])
        }
      />,
      <Column
        width={200}
        label="Price"
        dataKey="0"
        key={`${dataType}-price`}
        className={dataType === "asks" ? styles.redColor : styles.greenColor}
        cellDataGetter={({ rowData }) =>
          Intl.NumberFormat("en-IN", {
            minimumFractionDigits: 2,
          }).format(rowData[0])
        }
      />,
    ];

    return dataType === "bids" ? columns : columns.reverse();
  }, [dataType]);
  return (
    <Table
      width={600}
      height={500}
      headerHeight={40}
      rowHeight={30}
      rowCount={data.length}
      rowGetter={({ index }) => data[index]}
      gridClassName={`${styles.tableGrid} ${
        dataType === "bids" ? styles.invertDirection : ""
      }`}
      className={styles.table}
      headerRowRenderer={({ columns, className, style }) => (
        <div
          key={`${dataType}-header`}
          className={className}
          role="row"
          style={{
            ...style,
            paddingRight: dataType === "bids" ? 56 : 56,
            paddingLeft: dataType === "bids" ? 56 : 56,
          }}
        >
          {columns}
        </div>
      )}
      rowRenderer={({ key, rowData, columns, className, style }) => (
        <div
          key={key}
          className={`${className} ${styles.row}`}
          role="row"
          style={{
            ...style,
            background: `linear-gradient(
    ${dataType === "asks" ? "to right" : "to left"},
			${dataType === "asks" ? "#3d202c" : "#19383a"} ${
              (rowData[2] * 100) / data[data.length - 1][2]
            }%,
    transparent 0
  )
`,
          }}
        >
          {columns}
        </div>
      )}
    >
      {columComponents}
    </Table>
  );
};

export default VirtualizedTable;
