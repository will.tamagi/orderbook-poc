import Image from "next/image";
import React, { useCallback, useEffect, useState } from "react";
import Select from "react-select";

import styles from "./OrderBook.module.css";
import VirtualizedTable from "./VirtualizedTable/VirtualizedTable";
import { btcGroupOption, ethGroupOption } from "./constants";

import { Message, useOrderBookStore } from "../../store/useOrderBookStore";

let socket: WebSocket;

type Market = "PI_XBTUSD" | "PI_ETHUSD";

export const BidsTable: React.FC = () => {
  const groupedBids = useOrderBookStore((s) => s.groupedBids);

  return <VirtualizedTable data={groupedBids} dataType="bids" />;
};

export const AsksTable: React.FC = () => {
  const groupedAsks = useOrderBookStore((s) => s.groupedAsks);

  return <VirtualizedTable data={groupedAsks} dataType="asks" />;
};

const OrderBook: React.FC = () => {
  const [market, setMarket] = useState<Market>("PI_XBTUSD");
  const [socketClosed, setSocketClosed] = useState(false);
  const [socketErrorMessage, setSocketErrorMessage] = useState("");

  const updateSubscription = useOrderBookStore((s) => s.updateSubscription);
  const group = useOrderBookStore((s) => s.group);
  const setGroup = useOrderBookStore((s) => s.setGroup);
  const resetAsksBids = useOrderBookStore((s) => s.resetAsksBids);

  useEffect(() => {
    if (socketErrorMessage !== "") {
      setTimeout(() => setSocketErrorMessage(""), 1500);
    }
  }, [socketErrorMessage]);

  useEffect(() => {
    socket = new WebSocket("wss://www.cryptofacilities.com/ws/v1");
  }, []);

  useEffect(() => {
    socket.onopen = function () {
      setSocketErrorMessage("");
      socket?.send(
        JSON.stringify({
          event: "subscribe",
          feed: "book_ui_1",
          product_ids: [market],
        })
      );
    };

    socket.onclose = function () {
      resetAsksBids();
      setSocketErrorMessage(
        'Got an unexpected error, please try to refresh your page or click in the "Kill Feed" button.'
      );
    };
    socket.onerror = function () {
      setSocketErrorMessage("Got an unexpected error");
    };

    socket.onmessage = function (event) {
      const message: Message = JSON.parse(event.data);
      if (message.event === "error" || message.event === "alert") {
        setSocketErrorMessage(message?.message ?? "");
        setTimeout(() => setSocketErrorMessage(""), 1000);
        return;
      }

      if (message?.feed === "book_ui_1_snapshot") {
        resetAsksBids();
      }
      if (message?.asks !== undefined || message?.bids !== undefined) {
        updateSubscription(message);
      }
    };

    return () => {
      if (socket?.readyState === 1) {
        socket.send(
          JSON.stringify({
            event: "unsubscribe",
            feed: "book_ui_1",
            product_ids: [market],
          })
        );
        resetAsksBids();
      }
    };
  }, [socket]);

  const handleToggleFeed = useCallback(() => {
    socket.send(
      JSON.stringify({
        event: "unsubscribe",
        feed: "book_ui_1",
        product_ids: [market],
      })
    );

    setMarket((s) => {
      if (s === "PI_XBTUSD") {
        socket?.send(
          JSON.stringify({
            event: "subscribe",
            feed: "book_ui_1",
            product_ids: ["PI_ETHUSD"],
          })
        );
        setGroup(ethGroupOption[0]);
        return "PI_ETHUSD";
      }

      socket?.send(
        JSON.stringify({
          event: "subscribe",
          feed: "book_ui_1",
          product_ids: ["PI_XBTUSD"],
        })
      );
      setGroup(btcGroupOption[0]);
      return "PI_XBTUSD";
    });
  }, [socket, market]);

  const handleKillFeed = useCallback(() => {
    if (socketClosed) {
      socket = new WebSocket("wss://www.cryptofacilities.com/ws/v1");
      setSocketClosed(false);
    } else {
      socket.close();
      setSocketClosed(true);
    }
  }, [socket, socketClosed]);

  return (
    <div className={styles.orderBookWrapper}>
      <div className={styles.toolbar}>
        Order Book
        <Select
          instanceId="group-select"
          className={styles.select}
          value={group}
          onChange={setGroup}
          options={market === "PI_XBTUSD" ? btcGroupOption : ethGroupOption}
          isDisabled={socketClosed}
        />
      </div>
      <div className={styles.tablesWrapper}>
        <BidsTable />
        <AsksTable />
      </div>
      <div className={styles.footer}>
        <button
          className={styles.toggleFeedButton}
          onClick={handleToggleFeed}
          disabled={socketClosed}
        >
          <Image
            src="/switchArrowIcon.svg"
            alt="switch icon"
            width={20}
            height={20}
          />
          Toggle Feed
        </button>
        <button className={styles.killFeedButton} onClick={handleKillFeed}>
          <Image
            src="/exclamationIcon.svg"
            alt="exclamation icon"
            width={20}
            height={20}
          />
          Kill Feed
        </button>
      </div>
      {socketErrorMessage !== "" ? (
        <div className={styles.errorMessage}>{socketErrorMessage}</div>
      ) : null}
    </div>
  );
};

export default OrderBook;
