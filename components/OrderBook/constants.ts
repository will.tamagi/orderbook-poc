interface GroupOption {
  value: number;
  label: string;
}
export const btcGroupOption: GroupOption[] = [
  { value: 0.5, label: "Group 0.50" },
  { value: 1.0, label: "Group 1.00" },
  { value: 2.5, label: "Group 2.50" },
];

export const ethGroupOption: GroupOption[] = [
  { value: 0.05, label: "Group 0.05" },
  { value: 0.1, label: "Group 0.10" },
  { value: 0.25, label: "Group 0.25" },
];
