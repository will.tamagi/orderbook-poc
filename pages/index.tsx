import Head from "next/head";
import styles from "../styles/Home.module.css";

import OrderBook from "../components/OrderBook/OrderBook";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Order Book</title>
        <meta name="description" content="Order book simple poc" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <OrderBook />
      </main>
    </div>
  );
}
