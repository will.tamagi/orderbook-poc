import { Order } from "./useOrderBookStore";

export const groupOrders = (orders: Order[], group: number) => {
  if (group === 0.5 || group === 0.05) {
    return orders;
  }

  return orders.reduce<Order[]>((acc, curr) => {
    const normalizedPrice = Math.floor(curr[0] / group) * group;
    const arrLength = acc.length;
    if (arrLength !== 0 && acc[arrLength - 1][0] === normalizedPrice) {
      acc[arrLength - 1][1] = curr[1];
      acc[arrLength - 1][2] = curr[2];
    } else {
      acc.push([normalizedPrice, curr[1], curr[2]]);
    }
    return acc;
  }, []);
};

export const reconcileOrdersAsks = (
  prevOrders: Order[],
  updatedOrders: Order[]
): Order[] => {
  if (updatedOrders.length === 0) {
    return prevOrders;
  }
  const newOrders = [];

  let prevIndex = 0; // prevOrders index
  const prevOrdersLenght = prevOrders.length;

  let currIndex = 0; // updateOrders index
  const currOrdersLenght = updatedOrders.length;

  while (true) {
    if (prevIndex >= prevOrdersLenght && currIndex >= currOrdersLenght) {
      break;
    }

    const prevPrice = Number(prevOrders[prevIndex]?.[0]);
    const currPrice = Number(updatedOrders[currIndex]?.[0]);

    const prevAmount = Number(prevOrders[prevIndex]?.[1]);
    const currAmount = Number(updatedOrders[currIndex]?.[1]);

    const lastTotal: number =
      newOrders.length === 0 ? 0 : newOrders[newOrders.length - 1][2];

    if (currIndex >= currOrdersLenght) {
      if (prevAmount > 0) {
        newOrders.push([prevPrice, prevAmount, lastTotal + prevAmount]);
      }

      prevIndex++;
      continue;
    }

    if (prevIndex >= prevOrdersLenght) {
      if (currAmount > 0) {
        newOrders.push([currPrice, currAmount, lastTotal + currAmount]);
      }

      currIndex++;
      continue;
    }

    if (prevPrice < currPrice) {
      if (prevAmount > 0) {
        newOrders.push([prevPrice, prevAmount, lastTotal + prevAmount]);
      }

      prevIndex++;
      continue;
    }

    if (prevPrice > currPrice) {
      if (currAmount > 0) {
        newOrders.push([currPrice, currAmount, lastTotal + currAmount]);
      }

      currIndex++;
      continue;
    }

    if (currAmount > 0) {
      newOrders.push([currPrice, currAmount, lastTotal + currAmount]);
    }

    prevIndex++;
    currIndex++;
  }

  return newOrders;
};

export const reconcileOrdersBids = (
  prevOrders: Order[],
  updatedOrders: Order[]
): Order[] => {
  if (updatedOrders.length === 0) {
    return prevOrders;
  }
  const newOrders = [];

  let prevIndex = 0; // prevOrders index
  const prevOrdersLenght = prevOrders.length;

  let currIndex = 0; // updateOrders index
  const currOrdersLenght = updatedOrders.length;

  while (true) {
    if (prevIndex >= prevOrdersLenght && currIndex >= currOrdersLenght) {
      break;
    }

    const prevPrice = Number(prevOrders[prevIndex]?.[0]);
    const currPrice = Number(updatedOrders[currIndex]?.[0]);

    const prevAmount = Number(prevOrders[prevIndex]?.[1]);
    const currAmount = Number(updatedOrders[currIndex]?.[1]);

    const lastTotal: number =
      newOrders.length === 0 ? 0 : newOrders[newOrders.length - 1][2];

    if (currIndex >= currOrdersLenght) {
      if (prevAmount > 0) {
        newOrders.push([prevPrice, prevAmount, lastTotal + prevAmount]);
      }

      prevIndex++;
      continue;
    }

    if (prevIndex >= prevOrdersLenght) {
      if (currAmount > 0) {
        newOrders.push([currPrice, currAmount, lastTotal + currAmount]);
      }

      currIndex++;
      continue;
    }

    if (prevPrice < currPrice) {
      if (currAmount > 0) {
        newOrders.push([currPrice, currAmount, lastTotal + currAmount]);
      }

      currIndex++;
      continue;
    }

    if (prevPrice > currPrice) {
      if (prevAmount > 0) {
        newOrders.push([prevPrice, prevAmount, lastTotal + prevAmount]);
      }

      prevIndex++;
      continue;
    }

    if (currAmount > 0) {
      newOrders.push([currPrice, currAmount, lastTotal + currAmount]);
    }

    prevIndex++;
    currIndex++;
  }

  return newOrders;
};
