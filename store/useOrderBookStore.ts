import { OptionTypeBase } from "react-select";
import create from "zustand";

import { groupOrders, reconcileOrdersAsks, reconcileOrdersBids } from "./util";

import { btcGroupOption } from "../components/OrderBook/constants";

export type Order = number[];

export interface Message {
  event?: string;
  asks?: Order[];
  bids?: Order[];
  feed?: string;
  product_ids?: string[];
  product_id?: string;
  timestamp?: number;
  seq?: number;
  tickSize?: number;
  message?: string;
}

interface State {
  asks: Order[];
  bids: Order[];
  groupedAsks: Order[];
  groupedBids: Order[];
  group: OptionTypeBase;
  setGroup: (newGroup: OptionTypeBase) => void;
  resetAsksBids: () => void;
  updateSubscription: (message: Message) => void;
}

export const useOrderBookStore = create<State>((set, get) => ({
  asks: [],
  bids: [],
  groupedAsks: [],
  groupedBids: [],
  group: btcGroupOption[0],
  setGroup: (newGroup) =>
    set((state) => ({
      group: newGroup,
      groupedAsks: groupOrders(state.asks, newGroup.value),
      groupedBids: groupOrders(state.bids, newGroup.value),
    })),
  resetAsksBids: () =>
    set(() => ({
      asks: [],
      bids: [],
      groupedAsks: [],
      groupedBids: [],
    })),
  updateSubscription: (message) => {
    let newBids = message.bids ?? [];
    newBids =
      message.feed === "book_ui_1_snapshot" ? newBids : newBids.reverse();

    const reconciledAsks = reconcileOrdersAsks(get().asks, message.asks ?? []);
    const reconciledBids = reconcileOrdersBids(get().bids, newBids);

    set((state) => ({
      asks: reconciledAsks,
      bids: reconciledBids,
      groupedAsks: groupOrders(reconciledAsks, state.group.value),
      groupedBids: groupOrders(reconciledBids, state.group.value),
    }));
  },
}));
