A simple PoC of an order book

## Getting Started

First, run the development server:

```bash 
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
 
To run the unit tests:

```bashf
yarn test
```
